<table width="95%" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="#7DA6D8" style="font-family: verdana, arial, helvetica; font-size: 10pt;">
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#FFFFFF" style="font-family: verdana, arial, helvetica; font-size: 10pt;">
        <?php if ($business_header) { ?>
        <tr valign="top">
          <td>
            <table width="100%" style="font-family: verdana, arial, helvetica; font-size: 10pt;">
              <tr>
                <td>
                  [site-logo]
                </td>
                <td nowrap="nowrap">
                  <!--[store-address]<br />[store-phone]-->
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <?php } ?>

        <tr valign="top">
          <td>

            <?php if ($thank_you_message) { ?>
            <p><b><?php echo t('Thanks for your order, [order-first-name]!'); ?></b></p>

            <?php if (isset($_SESSION['new_user'])) { ?>
            <p><b><?php echo t('An account has been created for you with the following details:'); ?></b></p>
            <p><b><?php echo t('Username:'); ?></b> [new-username]<br/>
            <b><?php echo t('Password:'); ?></b> [new-password]</p>
            <?php } ?>

            <p><b><?php echo t('Want to manage your order online?'); ?></b><br />
            <?php echo t('If you need to check the status of your order, please visit our home page at [store-link] and click on "My account" in the menu or login with the following link:'); ?>
            <br /><br />[site-login]</p>
            <?php } ?>

            <table cellpadding="4" cellspacing="0" border="0" width="100%" style="font-family: verdana, arial, helvetica; font-size: 10pt;">
              <tr>
                <td colspan="2" bgcolor="#7DA6D8">
                  <b><?php echo t('Purchasing Information:'); ?></b>
	              </td>
              </tr>
              <tr>
                <td colspan="2">

                  <table width="100%" cellspacing="0" cellpadding="0" style="font-family: verdana, arial, helvetica; font-size: 10pt;">
                    <tr>
                      <td valign="top" width="50%">
                        <b><?php echo t('From:'); ?></b><br />
                        [order-billing-address]<br />
                      </td>
                      <td valign="top" width="50%">
                        <?php if (uc_order_is_shippable($order)): ?>
													<b><?php echo t('To:'); ?></b><br />
													[order-shipping-address]<br />
												<?php endif; ?>
                      </td>
                    </tr>
                  </table>

                </td>
              </tr>

              <?php if ($order->gift_order_mark): ?>
							<tr>
                <td colspan="2" >
                  <b><?php echo t('Gift Message:'); ?></b>
                </td>
              </tr>
							<tr>
                <td colspan="2" >
                  <p><?php echo $order->gift_order_message; ?></p>
                </td>
              </tr>
							<tr><td>&nbsp;</td></tr>
							<?php endif; ?>

              <tr>
                <td colspan="2" bgcolor="#7DA6D8">
                  <b><?php echo t('Order Summary:'); ?></b>
                </td>
              </tr>

              <tr>
                <td colspan="2">

                  <table border="0" cellpadding="1" cellspacing="0" width="100%" style="font-family: verdana, arial, helvetica; font-size: 10pt;">
                    <tr>
                      <td nowrap="nowrap">
                        <b><?php echo t('Order #:'); ?></b>
                      </td>
                      <td width="98%">
                        [order-link]
                      </td>
                    </tr>

                    <tr>
                      <td colspan="2">
                        <br /><br /><b><?php echo t('Products on order:'); ?>&nbsp;</b>

                        <table width="100%" style="font-family: verdana, arial, helvetica; font-size: 10pt;">

                          <?php if (is_array($order->products)) {
                            foreach ($order->products as $product) { ?>
                          <tr>
                            <td valign="top" nowrap="nowrap">
                              <b><?php echo $product->qty; ?> x </b>
                            </td>
                            <td width="98%">
                              <b><?php echo $product->title; ?></b> 
                              <br />
                              <?php echo t('Model: ') . $product->model; ?><br />
                              <?php if (is_array($product->data['attributes']) && count($product->data['attributes']) > 0) {?>
                              <?php foreach ($product->data['attributes'] as $key => $value) {
                                echo '<li>'. $key .': '. $value .'</li>';
                              } ?>
                              <?php } ?>
                              <br />
                            </td>
                          </tr>
                          <?php }
                              }?>

                      </td>
                    </tr>
                  </table>

                </td>
              </tr>

              <?php if ($help_text || $email_text || $store_footer) { ?>
              <tr>
                <td colspan="2">
                  <hr noshade="noshade" size="1" /><br />

                  <?php if ($email_text) { ?>
                  <p><?php echo t('Thanks again for shopping with us.'); ?></p>
                  <?php } ?>

                  <?php if ($store_footer) { ?>
                  <p><b>[store-link]</b><br /><b>[site-slogan]</b></p>
                  <?php } ?>
                </td>
              </tr>
              <?php } ?>

            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</td>
</tr>
</table>
